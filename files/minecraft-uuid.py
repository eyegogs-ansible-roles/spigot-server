#!/usr/bin/env python3
import sys
import requests
import json

def main():
    user = sys.argv[1]
    url = "https://api.mojang.com/users/profiles/minecraft/" + user

    try:
        response = requests.get(url)
        data = response.json()
    except json.decoder.JSONDecodeError:
        print("Player not found", user)
        exit(1)

    uuid = ""
    for i in range(0, len(data["id"])):
        uuid += data["id"][i]
        if i == 7 or i == 11 or i == 15 or i == 19:
            uuid += "-"
    print(uuid)

if __name__ == "__main__":
    main()
